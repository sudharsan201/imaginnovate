import { Component, Input, OnInit, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'app-test',
  templateUrl: './test.component.html',
  styleUrls: ['./test.component.css']
})
export class TestComponent implements OnInit {

  @Input() public fromParent: any;
  @Output() public childData = new EventEmitter();


  constructor() { }

  ngOnInit(): void {
  }

  childEvent() {
    this.childData.emit('This is From Child')
  }

  

  

}
